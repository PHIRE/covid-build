FROM rocker/r-ver:4.0.3

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
    file \
    git \
    libapparmor1 \
    libclang-dev \
    libcurl4-openssl-dev \
    libedit2 \
    libssl-dev \
    lsb-release \
    psmisc \
    procps \
    python-setuptools \
    sudo \
    wget \
    libxml2-dev \
    libcairo2-dev \
    libsqlite-dev \
    libmariadbd-dev \
    libmariadbclient-dev \
    libpq-dev \
    libv8-dev \
    libssh2-1-dev \
    unixodbc-dev \
    libsasl2-dev \
    texlive-fonts-extra \
    libharfbuzz-dev \
    libfribidi-dev \
    libfreetype6-dev \
    libpng-dev \
    libtiff5-dev \
    libjpeg-dev \
    glpk-utils \
    libglpk-dev \
  && install2.r --error \
    --deps TRUE \
    tidyverse \
    dplyr \
    devtools \
    formatR \
    V8 \
    remotes \
    selectr \
    nimble \
    doParallel \
    extrafont \
    lubridate \
    data.table \
    here \
    pacman \
    rstan \
    RhpcBLASctl

COPY Raleway.zip .

RUN    sed -i 's/^# *\(fr_FR ISO-8859-1\)/\1/' /etc/locale.gen \
    && sed -i 's/^# *\(fr_FR.UTF-8 UTF-8\)/\1/' /etc/locale.gen \
    && locale-gen

RUN unzip Raleway.zip -d /usr/share/fonts/truetype/Raleway \
  && cp /usr/share/fonts/truetype/Raleway/Raleway-Medium.ttf /usr/share/fonts/truetype/Raleway/Raleway.ttf \
  && update-locale
