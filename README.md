# Build system for COVID related artefacts.

This repo aims at providing a transparent and simple way to build multiple artefacts from the covid19 github repository.

This image is used by the MCHI Gitlab to fit the transmission models.

## Build and install a local covidr image

This repo contains machine readable instructions on how to build the image. Once this "container image" is built and added to the local registry (automatic), it can be used.

The following instructions assume you are at the root of this repository.

```{bash}
# Build using past cache
docker build -t covidr .
# Clean build from a clean slate
docker build --no-cache --tag covidr .
```

Note. We could host this on a private docker image registry, but since a couple of steps requires to be compiled, I prefer to build it on the machine/architecture that's running it.

## Using the image

Then, a shallow (without the change history) copy of the required repo is obtained.
```{bash}
git clone --depth=1 https://github.com/arnaud-godin/covid19.git
cd covid19
```

Then we can prepare the files for the current environment, and run them 

```{bash}
bash ../prep_build.sh
docker run -i --rm -v $(pwd):/data covidr < run_model_quebec.R
```

## Setting up a new runner.

* Update machine to newest Ubuntu LTS.
* Install docker for linux (+ post-install steps)
* Create group for execution and data folder.
  - `sudo mkdir /data`
  - `sudo groupadd covidr`
  - `sudo chgrp -R covidr /data`
  - `sudo chmod -R 2775 /data`
  - `sudo usermod -a -G covidr aman`
  - `sudo usermod -a -G covidr malavv`
* Go to new working directory and load builder config.
  - `cd /data`
  - `git clone https://git.mchi.mcgill.ca/PHIRE/covid-build`
  - `docker build --no-cache --tag covidr covid-build/`

# Patch the covidr image
Sometimes, it is necessary to quickly patch the Docker image. Rebuilding from scratch takes about 30 minutes. You can start an interactive shell into either bash or R and install a package (or whatever), and then save the changes with these commands:

```{bash}
# Opens an interactive shell in a new container from image
sudo docker run -it covidr /bin/bash
# OR: Opens an interactive R session in a new container from image
sudo docker run -it covidr R

# Install the package or whatever then exit or q()

# Write the image back
sudo docker commit `sudo docker ps -q -l` covidr
```

